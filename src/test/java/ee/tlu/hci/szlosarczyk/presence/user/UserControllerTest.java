package ee.tlu.hci.szlosarczyk.presence.user;

import com.fasterxml.jackson.databind.ObjectMapper;
import ee.tlu.hci.szlosarczyk.presence.Application;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by Maciej Szlosarczyk on 3/7/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class) @SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration public class UserControllerTest {

    private MockMvc mockMvc;

    @Autowired private UserRepository userRepository;

    @Autowired ObjectMapper objectMapper;

    @Autowired private WebApplicationContext webApplicationContext;

    /**
     * Remove all the users to avoid complications
     * Create a test user
     *
     * @throws Exception
     */
    @Before public void setUp() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        this.userRepository.deleteAll();
        this.userRepository.save(new User("My test User", "mytestemail@gmail.com", "my password"));
        this.userRepository
            .save(new User("My second test User", "mysecondTestUser@gmail.com", "my password"));
        this.userRepository
            .save(new User("My third testing user", "mythirdtestingUser@gmail.com", "my password"));
        this.userRepository.save(
            new User("My fourth testing user", "myfourthTestingUser@gmail.com", "my password"));
        this.userRepository
            .save(new User("My fifth testing user", "myfifthTestingUser@gmail.com", "my password"));

        System.out.print(userRepository.toString());
    }

    /**
     * Run a post request on the /users to create a new user.
     *
     * @throws Exception
     */
    @Test public void createUser() throws Exception {
        User newUser = new User("My name", "email@email.com", "my password");
        this.mockMvc.perform(post("/users/").contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(newUser))).andExpect(status().isCreated());
    }

    /**
     * Run a GET on an non-existent API and expect 404.
     *
     * @throws Exception
     */
    @Test public void userNotFound() throws Exception {
        mockMvc.perform(get("/users/66")).andExpect(status().isNotFound());
    }

    /**
     * Run a GET on the user list, expect 2 items to show up.
     *
     * @throws Exception
     */
    @Test public void getUserList() throws Exception {
        mockMvc.perform(get("/users")).andExpect(status().isOk())
            .andExpect(jsonPath("$[0].name").value("My test User"))
            .andExpect(jsonPath("$[1].name").value("My second test User"));

    }

    /**
     * Run a PUT on an existing user, expect updated user.
     * This test still fails randomly for no apparent reason.
     *
     * @throws Exception
     */
    @Test public void UpdateUser() throws Exception {
        User updateUser = new User("My updated name", "MyNewEmail@gmail.com", "my new password");
        mockMvc.perform(put("/users/3").contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(updateUser))).andExpect(status().isOk())
            .andExpect(jsonPath("$.name").value(("My updated name")));
    }

    /**
     * Run a DELETE on an existing user, expect a string.
     *
     * @throws Exception
     */
    @Test public void DeleteUser() throws Exception {
        mockMvc.perform(delete("/users/4")).andExpect(status().isOk())
            .andExpect(content().string(containsString("Your user has been deleted: ")))
            .andExpect(content().string(containsString("myfourthTestingUser@gmail.com")));
    }

    /**
     * @throws Exception
     */
    @Test public void correctUserFound() throws Exception {

        mockMvc.perform(get("/users/5")).
            andExpect(status().isOk()).
            andExpect(jsonPath("$.name").value(("My fifth testing user")));
    }
}
