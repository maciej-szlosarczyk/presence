package ee.tlu.hci.szlosarczyk.presence.blog.category;

import com.fasterxml.jackson.databind.ObjectMapper;
import ee.tlu.hci.szlosarczyk.presence.Application;
import ee.tlu.hci.szlosarczyk.presence.blog.Slug;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by Maciej_Szlosarczyk on 31/03/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class) @SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration public class CategoryControllerTest {

    private MockMvc mockMvc;

    @Autowired private CategoryService categoryService;

    @Autowired private CategoryRepository categoryRepository;

    @Autowired ObjectMapper objectMapper;

    @Autowired private WebApplicationContext webApplicationContext;

    @Before public void setUp() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        this.categoryRepository.deleteAll();
        this.categoryRepository
            .save(new Category("My category name", Slug.toSlug("My category name")));
        this.categoryRepository
            .save(new Category("My new testing category", Slug.toSlug("My new testing category")));
    }

    /**
     * Run a GET on the list of categories, expect 2 objects back.
     *
     * @throws Exception
     */
    @Test public void getCategories() throws Exception {
        mockMvc.perform(get("/categories")).andExpect(status().isOk())
            .andExpect(jsonPath("$[0].name").value("My category name"))
            .andExpect(jsonPath("$[1].slug").value("my-new-testing-category"));
    }

    /**
     * Run a POST on the list of categories, create a new one.
     *
     * @throws Exception
     */
    @Test public void createCategory() throws Exception {
        String newCategory = "{\"name\":\"Potato category\"}";
        mockMvc.perform(
            post("/categories").contentType(MediaType.APPLICATION_JSON).content(newCategory))
            .andExpect(jsonPath("$.name").value(("Potato category")))
            .andExpect(jsonPath("$.slug").value("potato-category"));
    }

    /**
     * Run a GET on non-existent category, get 404.
     *
     * @throws Exception
     */
    @Test public void categoryNotFound() throws Exception {
        mockMvc.perform(get("/categories/66")).andExpect(status().isNotFound());
    }

    /**
     * Run a GET on existing category, expect a correct response back.
     *
     * @throws Exception
     */
    @Test public void correctCategoryFound() throws Exception {
        mockMvc.perform(get("/categories/2")).andExpect(status().isOk())
            .andExpect(jsonPath("$.slug").value("my-new-testing-category"));
    }

    /**
     * Run a PUT on existing category, expect correct response back.
     *
     * @throws Exception
     */
    @Test public void updateCategory() throws Exception {
        String newName = "{\"name\":\"I'm testing a new name here\"}";
        ResultActions resultActions = mockMvc
            .perform(put("/categories/2").contentType(MediaType.APPLICATION_JSON).content(newName))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.name").value(("I'm testing a new name here")))
            .andExpect(jsonPath("$.slug").value("my-new-testing-category"));
    }

    @Test public void deleteCategory() throws Exception {
        mockMvc.perform(delete("/categories/1")).andExpect(status().isOk());
    }
}
