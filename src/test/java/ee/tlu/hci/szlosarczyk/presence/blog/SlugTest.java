package ee.tlu.hci.szlosarczyk.presence.blog;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Maciej_Szlosarczyk on 11/03/2016.
 */
public class SlugTest {

    @Test public void testToSlug() {
        String expectedSlug = "my-slug-is-here-1234";
        String slugifyThisString = "My slug is here 1234!!@#%#%^^.";
        Assert.assertTrue(expectedSlug.contentEquals(Slug.toSlug(slugifyThisString)));
    }


}
