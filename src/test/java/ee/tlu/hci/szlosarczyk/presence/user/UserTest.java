package ee.tlu.hci.szlosarczyk.presence.user;

import org.junit.Test;

import static org.junit.Assert.assertTrue;
/**
 * Created by Maciej_Szlosarczyk on 08/03/2016.
 */
public class UserTest {

    /**
     * Test toString method.
     */
    @Test public void testToString() {
        User user = new User("My test user", "myemail@gmail.com", "my password");
        String expected = "User{id=null, name='My test user', email='myemail@gmail.com'";
        System.out.print(user.toString());
        assertTrue(user.toString().contains(expected));
    }
}
