package ee.tlu.hci.szlosarczyk.presence.blog.post;

import com.fasterxml.jackson.databind.ObjectMapper;
import ee.tlu.hci.szlosarczyk.presence.Application;
import ee.tlu.hci.szlosarczyk.presence.blog.category.CategoryService;
import ee.tlu.hci.szlosarczyk.presence.user.User;
import ee.tlu.hci.szlosarczyk.presence.user.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by maciej_szlosarczyk on 04-Apr-16.
 * FIXME: All the tests are broken.
 */
@RunWith(SpringJUnit4ClassRunner.class) @SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration public class PostControllerTest {

    private MockMvc mockMvc;

    @Autowired private PostService postService;

    @Autowired private CategoryService categoryService;

    @Autowired private UserService userService;

    @Autowired private CommentService commentService;

    @Autowired private ObjectMapper objectMapper;

    @Autowired private WebApplicationContext webApplicationContext;

    @Before public void setUp() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        this.categoryService.createCategory("My new category");
        
        this.userService.createUser(new User("My user", "My email", "my password"));

        this.postService.createPost("Lorem Ipsum", "Lorum Ipsum post text.",
            this.categoryService.getSingleCategoryById(1), this.userService.getSingleUserById(1));

        this.commentService
            .createNewComment("My new comment", "My new email", "My new comment's text",
                postService.getSinglePostbyId(1));
    }

    /**
     * Create a new test category, and then create a new test post.
     *
     * @throws Exception
     */
    @Test public void createCategoryAndAPost() throws Exception {
        String newPost = "{\n" + "    \"title\":\"my title\",\n" + "    \"text\":\"my text\",\n"
            + "    \"category\": {\"name\":\"Potato category\"}\n" + "}";

        mockMvc.perform(post("/posts").contentType(MediaType.APPLICATION_JSON).content(newPost))
            .andExpect(jsonPath("$.text").value("my text"))
            .andExpect(jsonPath("$.category.id").value(2));

    }

    /**
     * Run a GET on posts.
     *
     * @throws Exception
     */
    @Test public void getPosts() throws Exception {
        mockMvc.perform(get("/posts/")).andExpect(status().isOk()).andExpect(status().isOk())
            .andExpect(jsonPath("$[0].name").value("Lorem Ipsum"));
    }

    /**
     * Run a GET on an unexisting post.
     *
     * @throws Exception
     */
    @Test public void postNotFound() throws Exception {
        mockMvc.perform(get("/posts/66")).andExpect(status().isNotFound());
    }

    @Test public void correctPostFound() throws Exception {
        mockMvc.perform(get("/posts/1")).andExpect(status().isOk())
            .andExpect(jsonPath("$.name").value("Lorem Ipsum"))
            .andExpect(jsonPath("$.text").value("Lorem Ipsum post text."));
    }
}
