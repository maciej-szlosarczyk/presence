package ee.tlu.hci.szlosarczyk.presence.blog.category;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ee.tlu.hci.szlosarczyk.presence.blog.post.Post;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Maciej Szlosarczyk on 3/10/2016.
 */
@Entity public class Category {
    @Column(name = "category_id", nullable = false) private Long id;

    @Column(nullable = false) private String name;

    @Column(nullable = false) private String slug;

    private List<Post> posts;

    // Constructors

    public Category() {
    }

    public Category(String name, String slug) {
        this.name = name;
        this.slug = slug;
    }

    // Adding posts to existing categories

    public void addPost(Post post) {
        this.posts.add(post);
        post.setCategory(this);
    }

    // Getters and setters

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    @JsonIgnore @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "category")
    public List<Post> getPosts() {
        return posts;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }

    // toString


    @Override public String toString() {
        final StringBuffer sb = new StringBuffer("Category{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", slug='").append(slug).append('\'');
        sb.append(", posts=").append(posts);
        sb.append('}');
        return sb.toString();
    }
}
