package ee.tlu.hci.szlosarczyk.presence.config;

import ee.tlu.hci.szlosarczyk.presence.user.User;
import ee.tlu.hci.szlosarczyk.presence.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

/**
 * Created by maciej_szlosarczyk on 24-Apr-16.
 */
@Component public class PreStart implements ApplicationListener<ContextRefreshedEvent> {
    @Autowired UserService userService;


    /**
     * On start, look for the administrator user in database. If found, do nothing. Otherwise, create one.
     *
     * @param event ContextRefershedEvent
     */
    @Override public void onApplicationEvent(final ContextRefreshedEvent event) {
        // Replace with your user data
        User administrator = userService.getSingleUserByEmail("administrator@presence.ee");
        String password = "password";


        if (administrator == null) {
            User newAdministrator =
                new User("administrator", "administrator@presence.ee", password);
            userService.createUser(newAdministrator);
        }
    }
}
