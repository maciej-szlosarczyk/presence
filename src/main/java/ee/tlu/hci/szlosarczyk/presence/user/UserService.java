package ee.tlu.hci.szlosarczyk.presence.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Maciej_Szlosarczyk on 05/04/2016.
 */
@Service public class UserService {

    @Autowired private UserRepository repository;

    public User createUser(User user) {
        return repository.save(user);
    }

    public List<User> getAllUsers() {
        return repository.findAll();
    }

    public User getSingleUserByName(String name) {
        return repository.findByName(name);
    }

    public User getSingleUserByEmail(String email) {
        return repository.findByEmail(email);
    }

    public User getSingleUserById(long id) {
        User user = repository.findOne(id);
        return user;
    }

    public void deleteUser(long id) {
        repository.delete(id);
    }

    public User updateUser(long id, User user) {
        user.setId(id);
        return repository.saveAndFlush(user);
    }
}
