package ee.tlu.hci.szlosarczyk.presence.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by maciej_szlosarczyk on 17-Apr-16.
 */
@Component public class CustomAuthenticationProvider implements AuthenticationProvider {

    @Autowired UserService userService;

    /**
     * @param authentication
     * @return If the user exists, authorize him with the role of an administrator.
     * @throws AuthenticationException
     */
    @Override public Authentication authenticate(Authentication authentication)
        throws AuthenticationServiceException {

        String username = authentication.getName();
        String password = authentication.getCredentials().toString();

        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        Authentication auth =
            new UsernamePasswordAuthenticationToken(username, password, grantedAuthorities);

        User user = userService.getSingleUserByEmail(username);

        if (user == null) {
            throw new AuthenticationServiceException("Authentication failed");
        }

        /**
         * Check if the password matches the saved hash.
         */
        if (!user.comparePasswords(password, user.getPasswordHash())) {
            throw new AuthenticationServiceException("Authentication failed.");
        } else {
            grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
            return auth;
        }
    }

    /**
     * @param authentication
     * @return UsernamePasswordAuthenticationToken.class
     */
    @Override public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
