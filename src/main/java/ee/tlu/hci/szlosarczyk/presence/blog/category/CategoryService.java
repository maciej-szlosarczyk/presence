package ee.tlu.hci.szlosarczyk.presence.blog.category;

import ee.tlu.hci.szlosarczyk.presence.blog.Slug;
import ee.tlu.hci.szlosarczyk.presence.blog.post.Post;
import ee.tlu.hci.szlosarczyk.presence.blog.post.PostService;
import ee.tlu.hci.szlosarczyk.presence.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by maciej_szlosarczyk on 03-Apr-16.
 */
@Service public class CategoryService {

    @Autowired private CategoryRepository repository;
    @Autowired private PostService postService;

    /**
     * @param name The name of the category
     * @return An unique slug for a category
     */
    public String negotiateUniqueSlug(String name) {
        int i = 1;
        String proposedSlug = Slug.toSlug(name);
        while (repository.findBySlug(proposedSlug) != null) {
            if (proposedSlug.contains("-(\\d+)$")) {
                proposedSlug.replaceAll("-(\\d+)$", "-" + i);
            } else {
                proposedSlug = Slug.toSlug(name + " " + i);
            }
            i++;
        }
        return proposedSlug;
    }


    public List<Category> getAllCategories() {
        return repository.findAll();
    }

    public Category createCategory(String name) {
        String slug = negotiateUniqueSlug(name);
        return repository.save(new Category(name, slug));
    }

    public Category getSingleCategoryBySlug(String slug) {
        return repository.findBySlug(slug);
    }

    public Category getSingleCategoryById(long id) {
        Category category = repository.findOne(id);
        return category;
    }

    public List<Post> getPostsByCategory(Category category) {
        return postService.getPostsByCategory(category);
    }

    public void deleteCategory(long id) {
        repository.delete(id);
    }

    public Category updateCategory(long id, Category category) {
        Category existingCategory = repository.findOne(id);
        existingCategory.setName(category.getName());
        return repository.saveAndFlush(existingCategory);
    }
}
