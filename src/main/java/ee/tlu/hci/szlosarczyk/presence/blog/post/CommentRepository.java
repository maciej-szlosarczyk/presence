package ee.tlu.hci.szlosarczyk.presence.blog.post;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Maciej_Szlosarczyk on 15/04/2016.
 */
public interface CommentRepository extends JpaRepository<Comment, Long> {
    List<Comment> findByPost(Post post);

    List<Comment> findByPostAndStatus(Post post, Comment.Status status);
}
