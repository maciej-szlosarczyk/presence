package ee.tlu.hci.szlosarczyk.presence.blog.post;

import ee.tlu.hci.szlosarczyk.presence.blog.category.Category;
import ee.tlu.hci.szlosarczyk.presence.user.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by maciej_szlosarczyk on 03-Apr-16.
 */
public interface PostRepository extends JpaRepository<Post, Long> {

    Post findByTitle(String title);

    List<Post> findByCategory(Category category);

    List<Post> findByAuthor(User author);
}
