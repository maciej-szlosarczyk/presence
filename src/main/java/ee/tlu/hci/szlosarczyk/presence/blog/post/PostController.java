package ee.tlu.hci.szlosarczyk.presence.blog.post;

import ee.tlu.hci.szlosarczyk.presence.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by maciej_szlosarczyk on 03-Apr-16.
 */

@RequestMapping(value = "/posts") @RestController public class PostController {

    @Autowired PostService postService;
    @Autowired CommentService commentService;

    @RequestMapping(method = RequestMethod.GET) public @ResponseBody List<Post> getAllPosts() {
        return postService.getAllPosts();
    }

    /**
     * Creates a ne post by POST /posts
     *
     * @param post
     * @return The newly created post
     */
    @RequestMapping(method = RequestMethod.POST) @ResponseStatus(HttpStatus.CREATED) public
    @ResponseBody Post createPost(@RequestBody Post post) {
        return postService
            .createPost(post.getTitle(), post.getText(), post.getCategory(), post.getAuthor());
    }

    /**
     * Gets a single post by GET /posts/{id}
     *
     * @param id
     * @return
     * @throws ResourceNotFoundException
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET) public @ResponseBody
    Post getSinglePost(@PathVariable("id") long id) throws ResourceNotFoundException {
        if (postService.getSinglePostbyId(id) == null) {
            throw new ResourceNotFoundException();
        }
        return postService.getSinglePostbyId(id);
    }

    /**
     * Deletes a single post by DELETE /posts/{id}
     *
     * @param id
     * @return
     * @throws ResourceNotFoundException
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE) @ResponseBody String deletePost(
        @PathVariable("id") long id) throws ResourceNotFoundException {
        Post post = postService.getSinglePostbyId(id);
        if (post == null) {
            throw new ResourceNotFoundException();
        }
        postService.deletePost(id);
        String message = "The following post has been deleted: " + post;
        return message;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT) @ResponseBody Post updatePost(
        @PathVariable("id") long id, @RequestBody Post post) throws ResourceNotFoundException {
        if (postService.getSinglePostbyId(id) == null) {
            throw new ResourceNotFoundException();
        }
        return postService.updatePost(id, post);

    }

    // Comments Start here

    @RequestMapping(value = "/{id}/comments", method = RequestMethod.GET) @ResponseBody
    List<Comment> getPublicCommentsByPost(@PathVariable("id") long postId)
        throws ResourceNotFoundException {
        Post post = postService.getSinglePostbyId(postId);
        if (post == null || commentService.getPublicCommentsByPost(post) == null) {
            throw new ResourceNotFoundException();
        }
        return commentService.getPublicCommentsByPost(post);
    }

    @RequestMapping(value = "/{id}/comments", method = RequestMethod.POST) @ResponseBody
    Comment createComment(@PathVariable("id") long postId, @RequestBody Comment comment)
        throws ResourceNotFoundException {
        Post post = postService.getSinglePostbyId(postId);
        if (post == null) {
            throw new ResourceNotFoundException();
        }
        return commentService
            .createNewComment(comment.getName(), comment.getEmail(), comment.getText(), post);
    }
}
