package ee.tlu.hci.szlosarczyk.presence.blog.post;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Maciej_Szlosarczyk on 15/04/2016.
 */
@Entity public class Comment {

    public enum Status {
        spam, ok
    }


    @Column private long id;

    @Column(nullable = false, length = 255) private String name;

    @Column(nullable = false, length = 255) private String email;

    @Column(nullable = false, length = 9999) private String text;

    @CreationTimestamp private Date creationDate;

    private Post post;

    private Status status;

    // Constructors

    public Comment() {

    }

    public Comment(String text, String name, String email, Post post) {
        this.text = text;
        this.name = name;
        this.email = email;
        this.creationDate = new Date();
        this.post = post;
        this.status = Status.ok;
    }

    // Getters and setters


    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST) @JoinColumn(name = "post_id")
    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    @Enumerated(EnumType.ORDINAL) public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    // toString

    @Override public String toString() {
        final StringBuilder sb = new StringBuilder("Comment{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", email='").append(email).append('\'');
        sb.append(", text='").append(text).append('\'');
        sb.append(", creationDate=").append(creationDate);
        sb.append(", post=").append(post);
        sb.append(", status=").append(status);
        sb.append('}');
        return sb.toString();
    }
}
