package ee.tlu.hci.szlosarczyk.presence.blog.post;

import ee.tlu.hci.szlosarczyk.presence.blog.category.Category;
import ee.tlu.hci.szlosarczyk.presence.blog.category.CategoryRepository;
import ee.tlu.hci.szlosarczyk.presence.blog.category.CategoryService;
import ee.tlu.hci.szlosarczyk.presence.exceptions.ResourceNotFoundException;
import ee.tlu.hci.szlosarczyk.presence.user.User;
import ee.tlu.hci.szlosarczyk.presence.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by maciej_szlosarczyk on 03-Apr-16.
 */
@Service public class PostService {
    @Autowired private PostRepository repository;
    @Autowired private CategoryService categoryService;
    @Autowired private CategoryRepository categoryRepository;
    @Autowired private UserService userService;

    /**
     * @return
     */
    public List<Post> getAllPosts() {
        return repository.findAll();
    }

    /**
     * @param title
     * @param text
     * @param category
     * @param user
     * @return
     * @throws ResourceNotFoundException
     */
    public Post createPost(String title, String text, Category category, User user)
        throws ResourceNotFoundException {
        Category onlyExistingCategory = categoryService.getSingleCategoryById(category.getId());
        User onlyExistingUser = userService.getSingleUserById(user.getId());

        if (onlyExistingCategory == null || onlyExistingUser == null) {
            throw new ResourceNotFoundException();
        }

        Post newPost = new Post(title, text, onlyExistingCategory, onlyExistingUser);
        return repository.save(newPost);
    }

    /**
     * @param id
     * @return
     */
    public Post getSinglePostbyId(long id) {
        return repository.findOne(id);
    }

    /**
     * @param category
     * @return
     */
    public List<Post> getPostsByCategory(Category category) {
        return repository.findByCategory(category);
    }

    /**
     * @param id
     */
    public void deletePost(long id) {
        repository.delete(id);
    }


    /**
     * @param id
     * @param post
     * @return
     */
    public Post updatePost(long id, Post post) {
        Post existingPost = repository.findOne(id);

        existingPost.setTitle(post.getTitle());
        existingPost.setText(post.getText());
        existingPost.setCategory(categoryService.getSingleCategoryById(post.getCategory().getId()));
        existingPost.setAuthor(userService.getSingleUserById(post.getAuthor().getId()));

        return repository.saveAndFlush(existingPost);
    }
}
