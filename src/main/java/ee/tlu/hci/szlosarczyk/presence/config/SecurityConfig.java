package ee.tlu.hci.szlosarczyk.presence.config;

import ee.tlu.hci.szlosarczyk.presence.user.CustomAuthenticationProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * Created by Maciej_Szlosarczyk on 15/04/2016.
 */
@Configuration @EnableWebSecurity public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired CustomAuthenticationProvider authenticationProvider;

    @Override protected void configure(AuthenticationManagerBuilder authenticationManagerBuilder)
        throws Exception {
        authenticationManagerBuilder.authenticationProvider(authenticationProvider);
    }


    @Override protected void configure(HttpSecurity httpSecurity) throws Exception {
        // @formatter:off
        httpSecurity.authorizeRequests()
            .antMatchers(HttpMethod.POST, "/posts/*/comments").permitAll()
            .antMatchers(HttpMethod.DELETE, "/**").authenticated()
            .antMatchers(HttpMethod.POST, "/**").authenticated()
            .antMatchers(HttpMethod.PUT, "/**").authenticated()
            .and().httpBasic()
            .and().csrf().disable();
        // @formatter:on
    }
}
