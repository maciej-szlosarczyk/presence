package ee.tlu.hci.szlosarczyk.presence.blog.category;

import ee.tlu.hci.szlosarczyk.presence.blog.post.Post;
import ee.tlu.hci.szlosarczyk.presence.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by maciej_szlosarczyk on 02-Apr-16.
 */
@RequestMapping(value = "/categories") @RestController public class CategoryController {

    @Autowired CategoryService service;

    @RequestMapping(method = RequestMethod.GET) public @ResponseBody
    List<Category> getAllCategories() {
        return service.getAllCategories();
    }

    @RequestMapping(method = RequestMethod.POST) @ResponseStatus(HttpStatus.CREATED) public
    @ResponseBody Category createCategory(@RequestBody Category category) {
        return service.createCategory(category.getName());
    }

    /**
     * @param id
     * @return A single selected category
     * @throws ResourceNotFoundException
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET) public @ResponseBody
    Category getSingleCategory(@PathVariable("id") long id) throws ResourceNotFoundException  {
         if (service.getSingleCategoryById(id) == null) {
             throw new ResourceNotFoundException();
         }
        return service.getSingleCategoryById(id);
    }

    /**
     *
     * @param id
     * @return
     * @throws ResourceNotFoundException
     */
    @RequestMapping(value = "/{id}/posts", method = RequestMethod.GET) public @ResponseBody
    List<Post> getPostsFromSingleCategoryById(@PathVariable("id") long id)
        throws ResourceNotFoundException {
        Category myCategory = service.getSingleCategoryById(id);
        if (myCategory == null) {
            throw new ResourceNotFoundException();
        }
        return service.getPostsByCategory(service.getSingleCategoryById(myCategory.getId()));
    }

    /**
     * @param id
     * @return Removed user as a text.
     * @throws ResourceNotFoundException
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE) @ResponseBody
    String deleteCategory(@PathVariable("id") long id) throws ResourceNotFoundException {
        Category category = service.getSingleCategoryById(id);
        if (category == null) {
            throw new ResourceNotFoundException();
        }
        service.deleteCategory(id);
        String message =
            "The following category has been deleted: " + category;
        return message;
    }

    /**
     * @param id
     * @return The updated category.
     * @throws ResourceNotFoundException
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT) public @ResponseBody
    Category updateCategory(@PathVariable("id") long id, @RequestBody Category category)
        throws ResourceNotFoundException {
        if (service.getSingleCategoryById(id) == null) {
            throw new ResourceNotFoundException();
        }
        return service.updateCategory(id, category);
    }
}
