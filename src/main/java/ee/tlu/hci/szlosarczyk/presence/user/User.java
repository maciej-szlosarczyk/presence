package ee.tlu.hci.szlosarczyk.presence.user;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.CreationTimestamp;
import org.mindrot.jbcrypt.BCrypt;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Maciej Szlosarczyk on 3/6/2016.
 */

@Entity public class User {
    @Column private Long id;

    @Column(nullable = false) private String name;

    @Column(nullable = false, unique = true) private String email;

    @Column(nullable = false) private String passwordHash;

    @CreationTimestamp private Date creationDate;

    // Constructors

    public User() {
    }

    @JsonCreator public User(@JsonProperty("name") String name, @JsonProperty("email") String email,
        @JsonProperty("password") String password) {
        this.name = name;
        this.email = email;
        this.creationDate = new Date();
        this.passwordHash = hashPassword(password);
    }

    // Additional methods

    /**
     * @param password
     * @return hashed password to store in the database
     */
    public String hashPassword(String password) {
        String hashedPassword = BCrypt.hashpw(password, BCrypt.gensalt(12));
        return hashedPassword;
    }

    public boolean comparePasswords(String candidate, String passwordHash) {
        if (BCrypt.checkpw(candidate, passwordHash)) {
            return true;
        } else
            return false;
    }

    // Getters and setters

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String password) {
        this.passwordHash = password;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    // toString

    @Override public String toString() {
        final StringBuffer sb = new StringBuffer("User{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", email='").append(email).append('\'');
        sb.append(", passwordHash='").append(passwordHash).append('\'');
        sb.append(", creationDate=").append(creationDate);
        sb.append('}');
        return sb.toString();
    }
}
