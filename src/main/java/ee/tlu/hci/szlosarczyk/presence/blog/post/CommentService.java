package ee.tlu.hci.szlosarczyk.presence.blog.post;

import ee.tlu.hci.szlosarczyk.presence.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Maciej_Szlosarczyk on 15/04/2016.
 */
@Service public class CommentService {
    @Autowired private CommentRepository commentRepository;
    @Autowired private PostService postService;

    /**
     *
     * @param id
     * @return
     * @throws ResourceNotFoundException
     */
    public Comment getCommentById(Long id) throws ResourceNotFoundException {
        if (commentRepository.findOne(id) == null) {
            throw new ResourceNotFoundException();
        }

        return commentRepository.findOne(id);
    }

    /**
     * @param post
     * @return
     */
    public List<Comment> getCommentsByPost(Post post) {
        return commentRepository.findByPost(post);
    }

    /**
     * @param post
     * @return
     */
    public List<Comment> getPublicCommentsByPost(Post post) {
        return commentRepository.findByPostAndStatus(post, Comment.Status.ok);
    }

    /**
     * @param name
     * @param title
     * @param text
     * @param post
     * @return
     * @throws ResourceNotFoundException
     */
    public Comment createNewComment(String name, String title, String text, Post post)
        throws ResourceNotFoundException {
        Post onlyExistingPost = postService.getSinglePostbyId(post.getId());

        if (onlyExistingPost == null) {
            throw new ResourceNotFoundException();
        }

        Comment newComment = new Comment(name, title, text, onlyExistingPost);

        return commentRepository.save(newComment);
    }

    /**
     * @param comment
     * @return
     * @throws ResourceNotFoundException
     */
    public Comment editComment(Comment comment) throws ResourceNotFoundException {
        Comment updatedComment = getCommentById(comment.getId());
        if (updatedComment == null) {
            throw new ResourceNotFoundException();
        }

        updatedComment.setName(comment.getName());
        updatedComment.setEmail(comment.getEmail());
        updatedComment.setText(comment.getText());
        updatedComment.setPost(comment.getPost());
        updatedComment.setStatus(comment.getStatus());

        return commentRepository.saveAndFlush(updatedComment);

    }

    /**
     * @param comment
     * @return
     * @throws ResourceNotFoundException
     */
    public Comment changeStatus(Comment comment) throws ResourceNotFoundException {
        Comment updatedComment = getCommentById(comment.getId());
        if (updatedComment == null) {
            throw new ResourceNotFoundException();
        }

        if (updatedComment.getStatus() == Comment.Status.ok) {
            updatedComment.setStatus(Comment.Status.spam);
        } else {
            updatedComment.setStatus(Comment.Status.ok);
        }

        return updatedComment;
    }
}
