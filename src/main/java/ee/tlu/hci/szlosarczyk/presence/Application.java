package ee.tlu.hci.szlosarczyk.presence;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by Maciej Szlosarczyk on 3/6/2016.
 */

@SpringBootApplication public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
