package ee.tlu.hci.szlosarczyk.presence.blog.category;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Maciej Szlosarczyk on 3/10/2016.
 */
public interface CategoryRepository extends JpaRepository<Category, Long> {
    /**
     * @param name
     * @return Return a with a given name.
     */
    Category findByName(String name);

    /**
     * @param slug
     * @return Returns a category with a given slug.
     */
    Category findBySlug(String slug);
}
