package ee.tlu.hci.szlosarczyk.presence.blog.post;

import ee.tlu.hci.szlosarczyk.presence.blog.category.Category;
import ee.tlu.hci.szlosarczyk.presence.user.User;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by maciej_szlosarczyk on 03-Apr-16.
 */

@Entity public class Post {
    @Column private Long id;

    @Column(nullable = false, length = 255) private String title;

    @Column(nullable = false, length = 999999) private String text;

    @CreationTimestamp private Date creationDate;

    private Category category;

    private User author;

    // Constructors

    public Post() {
    }

    public Post(String title, String text) {
        this.title = title;
        this.text = text;
    }

    public Post(String title, String text, Category category, User author) {
        this.title = title;
        this.text = text;
        this.category = category;
        this.creationDate = new Date();
        this.author = author;
    }

    // Getters and Setters


    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "category_id") public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST) @JoinColumn(name = "post_id")
    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    @Override public String toString() {
        final StringBuffer sb = new StringBuffer("Post{");
        sb.append("id=").append(id);
        sb.append(", title='").append(title).append('\'');
        sb.append(", text='").append(text).append('\'');
        sb.append(", creationDate=").append(creationDate);
        sb.append(", category=").append(category);
        sb.append(", author=").append(author);
        sb.append('}');
        return sb.toString();
    }
}
