package ee.tlu.hci.szlosarczyk.presence.user;

import org.springframework.data.jpa.repository.JpaRepository;


/**
 * Created by Maciej Szlosarczyk on 3/6/2016.
 */

public interface UserRepository extends JpaRepository<User, Long> {
    User findByEmail(String email);

    User findByName(String name);
}
