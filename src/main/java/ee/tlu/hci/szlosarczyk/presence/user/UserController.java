package ee.tlu.hci.szlosarczyk.presence.user;

import ee.tlu.hci.szlosarczyk.presence.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Maciej Szlosarczyk on 3/6/2016.
 */

@RequestMapping(value = "/users") @RestController public class UserController {

    @Autowired UserService service;

    /**
     * @return Return all registered users.
     */
    @RequestMapping(method = RequestMethod.GET) public @ResponseBody List<User> getAllUsers() {
        return service.getAllUsers();
    }

    /**
     * @return
     */
    @RequestMapping(method = RequestMethod.POST) @ResponseStatus(HttpStatus.CREATED) public
    @ResponseBody User createUser(@RequestBody User user) {
        return service.createUser(user);
    }

    /**
     * @param id
     * @return A string that your user has been deleted.
     * @throws ResourceNotFoundException
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE) public @ResponseBody
    String deleteUser(@PathVariable("id") long id) throws ResourceNotFoundException {
        if (service.getSingleUserById(id) == null) {
            throw new ResourceNotFoundException();
        }
        String deletedUser = service.getSingleUserById(id).toString();
        service.deleteUser(id);
        return "Your user has been deleted: " + deletedUser;
    }

    /**
     * @param id
     * @return A single selected user.
     * @throws ResourceNotFoundException
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET) public @ResponseBody
    User getSingleUser(@PathVariable("id") long id) throws ResourceNotFoundException {
        if (service.getSingleUserById(id) == null) {
            throw new ResourceNotFoundException();
        }
        return service.getSingleUserById(id);
    }

    /**
     * @param id
     * @param name
     * @param email
     * @return Newly updated user.
     * @throws ResourceNotFoundException
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT) public @ResponseBody
    User updateUser(@PathVariable("id") long id, @RequestBody User user)
        throws ResourceNotFoundException {
        if (service.getSingleUserById(id) == null) {
            throw new ResourceNotFoundException();
        }
        return service.updateUser(id, user);
    }
}
